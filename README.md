<!--
    vim: et: sw=4: ts=4: tw=0:
    vim: spell: spelllang=en:
-->
# Python application template

[![][badge-version]][repository-latest-release]

Item | Choices
--- | ---
Providers | [![][badge-provider-github]][Github] [![][badge-provider-gitlab]][Gitlab]
CI | [![][badge-ci-gitlab]][Gitlab CI]
Python | [![][badge-python3.5]][python3.5-docs] [![][badge-python3.6]][python3.6-docs] [![][badge-python3.7]][python3.7-docs]
License | [![][badge-mit]][MIT License]
Styles | [![][badge-style-black]][Black]
IDEs | [![][badge-ide-PyCharm]][PyCharm]

*A [cookiecutter] template for Python application development.*

**Features:**

* Packaging and dependency management with [Poetry]
* Easy uploads to [TestPyPI] and/or [PyPI]
* [Pre-commit] hooks that
    * Automate code styling with [Black]
    * Update the [README]'s table of contents
* [Gitlab CI] (continuous integration), featuring
    * pytest-based test suite with [Codecov]-powered [coverage reports][repository-codecov]
    * Builds the [Sphinx]-based documentation and deploys it to [Gitlab Pages] (defaults to https://pedroasad.com.gitlab.io/templates/python/python-app/)
    * Uploads successfully built, tagged distributions to [TestPyPI] (defaults to https://test.pypi.org/project/pedroasad-template-python-app)
* Version bumping/tagging with [bumpversion]

For a full description of the repository contents (files and directories), refer to the [MANIFEST], and for the history of changes, refer to the [CHANGELOG].

---

**Table of contents:**

[](TOC)

- [Python application template](#python-application-template)
  - [Using the template](#using-the-template)
    - [Final instructions](#final-instructions)
  - [Cookiecutter variables](#cookiecutter-variables)
  - [Limitations](#limitations)
  - [Future directions](#future-directions)
  - [Further references](#further-references)

[](TOC)

---

## Using the template

[Cookiecutter] takes a properly structured Git repository, asks a few questions (like author name, project name, project URL, *etc.*) and gives you a fresh project template customized to your answers.
The only requirement for using a [cookiecutter] template is the `cookiecutter` package itself, but since the project's dependencies and packaging are managed with [Poetry], you'll also need to install it.
You may install them however if you prefer (*e.g.* `pip install --user cookiecutter poetry`), then instantiate the template like this:

```bash
cookiecutter https://gitlab.com/pedroasad.com/templates/python/python-app.git 
```

You'll be prompted for a handful of questions, described in [Cookiecutter Variables](#cookiecutter-variables).
After you answer all of them,

1. Your customized project will be created in a new directory,
1. [Poetry] will 
   1. Create a virtualenv (depending on you settings), 
   1. Update dependencies, generating a new lock-file (`poetry.lock`),
   1. Install the project in editable mode, and
   1. Compile the HTML package documentation, which will be placed in `docs/_build/html`,
1. A new Git repository will be initiallized,
   * With an `origin` remote pointing to a repository URL determined from you answers (see [Cookiecutter Variables](#cookicutter-variables])), and
   * All generated files will be added to it,
1. [pre-commit] will be installed and run to fix the README's and CONTRIBUTING's tables of contents.

### Final instructions

1. Assuming you have not yet created a repository on Github/Gitlab, create one, obeying the *organization name* and *group name* that you informed before (see [Cookiecutter Variables](#cookicutter-variables]) below).
1. If you want to get [Codecov] reports, you should
1. If you whish you package published to [TestPyPI] by [Gitlab CI] (as specified in the `.gitlab-ci.yml` file), you should
   1. Create an account on [TestPyPI], and
   1. Set up the `TESTPYPI_USERNAME` and `TESTPYPI_PASSWORD` on your project's [CI settings](https://docs.gitlab.com/ee/user/project/pipelines/settings.html) page.
   Likewise, if you want your package directly published to [PyPI] you should adjust `.gitlab-ci.yml` and set up any relevant variables.
1. Review the generated `README.md` for additional project usage instructions, which you may or may not want to keep.
1. The contents will be added to Git, but not committed, and no tags will be created.
   It is up to you to review the contents, `git commit`, then `git tag` with a tag that reflects the initial version you informed.
   After you assign the first tag, create a `release` branch into which you'll merge tagged commits.
   The [CHANGELOG] lists unreleased changes as the diff between the `release` and `master` branches.


## Cookiecutter variables

These are the questions asked by [`cookiecutter.json`](cookiecutter.json):

Variable | Meaning | Default value
--- | --- | ---
`author_name` | Main author's full name/nickname | Pedro Asad,
`author_email` | Main author's e-mail | pasad@lcg.ufrj.br,
`python_version` | Either 3.5, 3.6, or 3.7 | 3.7
`package_name` | Python-impotable package name &mdash; `import {{package_name}}` | app
`distribution_name` | Package name on [PyPI]/[TestPyPI] &mdash; `pip install {{distribution_name}`| pedroasad-templates-python-app
`version` | Initial project version | 0.1.0
`license` | Project's license (only [MIT License] is currently supported | MIT
`one_liner` | Brief project description | Template Python application.
`repo_provider` | Choose between Github or Gitlab (but only [Gitlab CI] is currently supported) | gitlab
`repo_organization` | If a personal project, this is your Github/Gitlab user-name | pedroasad.com
`repo_group` | An optional namespace inside the organization | templates/python
`repo_name` | Project slug, the final part of the project's URL | python-app

The actual repository URL is formed by the junction of the provider's URL, the organization name, the group (if any), and the project's name.
For instance, with the default values above, we get

What | Rule | Value
--- | --- | ---
Project's URL | `https://{{repo_provider}}.com/{{repo_organization}}/{{repo_group}}/{{repo_name}}` | https://gitlab.com/lcg/neuro/python-plx/
`origin` Git remote | `git@{{repo_provider}}.com:{{repo_organization}}/{{repo_group}}/{{repo_name}}.git` | git@gitlab.com:pedroasad.com/templates/python/python-app.git
[Gitlab Pages] URL | `https://{{repo_organization}}.{{repo_provider}}.io/{{repo_group}}/{{repo_name}}/` | https://pedroasad.com.gitlab.io/templates/python/python-app/


## Limitations

* It is currently not possible to choose a differente license than [MIT's][MIT License].
* Although you may choose `"github"` as your provider, it only [Gitlab CI] is setup (but [Github] uses [Travis]).
* Tables of contents are not correctly parsed by [Sphinx] when building the documentation.
* Markdown tables are not correctly parsed by [Sphinx] when building the documentation.
   
   
## Future directions

* Add useful Python tasks (via [pyinvoke]) and more [pre-commit] hooks
* Detail the [Sphinx]-based docs
* Allow the user to choose a license
* Set up Tox test suite
* Set up Docker deployment
* Integrate Docker deployment into CI
* Add a best practices and contribution guides
* Allow to choose between multiple linters/code formatters
* Create basic test suite
   
## Further references

* [Current State of Python Packaging](https://stefanoborini.com/current-status-of-python-packaging/)
* [How to package your Python code](https://python-packaging.readthedocs.io/en/latest/index.html)

---

*&mdash; Powered by [Gitlab CI]*

[Black]: https://black.readthedocs.io/
[CHANGELOG]: ./CHANGELOG.md
[Codecov]: https://codecov.io
[Github]: https://github.com
[Gitlab CI]: https://docs.gitlab.com/ee/ci
[Gitlab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[Jinja2]: http://jinja.pocoo.org/docs/2.10/
[MANIFEST]: ./MANIFEST.md
[MIT License]: https://opensource.org/licenses/MIT
[Poetry]: https://github.com/sdispater/poetry
[PyCharm]: https://www.jetbrains.com/pycharm
[PyPI]: https://pypi.org
[README]:  https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/README.md
[Sphinx]: https://www.sphinx-doc.org/en/master/index.html
[TestPyPI]: https://test.pypi.org
[Travis]: https://travis-ci.org
[Twine]: https://twine.readthedocs.io
[badge-ci-gitlab]: https://img.shields.io/badge/CI-Gitlab%20CI-blue.svg
[badge-ide-pycharm]: https://img.shields.io/badge/IDE-PyCharm-yellow.svg
[badge-mit]: https://img.shields.io/badge/license-MIT-blue.svg
[badge-provider-github]: https://img.shields.io/badge/Provider-Github-blue.svg
[badge-provider-gitlab]: https://img.shields.io/badge/Provider-Github-blue.svg
[badge-python3.5]: https://img.shields.io/badge/Python-3.5-blue.svg
[badge-python3.6]: https://img.shields.io/badge/Python-3.6-blue.svg
[badge-python3.7]: https://img.shields.io/badge/Python-3.7-blue.svg
[badge-style-black]: https://img.shields.io/badge/Style-Black-black.svg
[badge-version]: https://img.shields.io/badge/version-1.1.0%20(alpha)-orange.svg
[bumpversion]: https://pypi.org/project/bumpversion/
[cookiecutter]: https://cookiecutter.readthedocs.io
[gitlab]: https://gitlab.com
[pre-commit]: https://pre-commit.com/
[pyinvoke]: http://www.pyinvoke.org/
[pytest]: https://pytest.org/
[python-setup-post]: https://towardsdatascience.com/10-steps-to-set-up-your-python-project-for-success-14ff88b5d13
[python3.5-docs]: https://docs.python.org/3.5
[python3.6-docs]: https://docs.python.org/3.6
[python3.7-docs]: https://docs.python.org/3.7
[repository-codecov]: https://codecov.io/gl/pedroasad.com:templates:python/python-app
[repository-latest-release]: https://test.pypi.org/project/pedroasad-template-python-app/1.1.0/
[repository-master]: https://gitlab.com/pedroasad.com/templates/python/python-app
[repository]: https://gitlab.com/pedroasad.com/templates/python/python-app
[virtualenv]: https://virtualenv.pypa.io/en/latest/

