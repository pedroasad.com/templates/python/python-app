<!--
    vim: et: sw=4: ts=4: tw=0:
    vim: spell: spelllang=en:
-->
# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].


## [Unreleased]
### Added

* A convenience [Vim modelines] preamble in all Markdown files.
* Post-generation hook updates `pip` before updating/installing dependencies.

### Changed

* Added minimal variable validation to prevent most obvious mistakes
* Initial Git configuration now sets up user name and email.
* Fixed bug in [`{{cookiecutter.repo_name}}/CONTRIBUTING.md`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master{{cookiecutter.repo_name}}/CONTRIBUTING.md) generation.

### Added

* [`{{cookiecutter.repo_name}}/post_gen_project.py`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/{{cookiecutter.repo_name}}/post_gen_project.py) &mdash; now, the actual post-generation hook is here, because we need access to the auxiliary template in `vars.jinja` (see the comments in the original `hooks/post_get_project.py` script and cookiecutter issues 706 and 851 for more details).
* Minimal support for PyCharm projects via the `ide` key and the files
    * [`{{cookiecutter.repo_name}}/.idea/misc.xml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/{{cookiecutter.repo_name}}/.idea/misc.xml)
    * [`{{cookiecutter.repo_name}}/.idea/modules.xml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/{{cookiecutter.repo_name}}/.idea/modules.xml)
    * [`{{cookiecutter.repo_name}}/.idea/vcs.xml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/{{cookiecutter.repo_name}}/.idea/vcs.xml)
    * [`{{cookiecutter.repo_name}}/.idea/{{cookiecutter.repo_name}}.iml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/{{cookiecutter.repo_name}}/.idea/{{cookiecutter.repo_name}}.iml)
    which are removed by the post-generation script if `"PyCharm"` is not chosen.
* [`{{cookiecutter.repo_name}}/vars.jinja`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/{{cookiecutter.repo_name}}/vars.jinja) with common template code, now included by other files.
* [`.gitignore`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/.gitignore), which lists:
    * PyCharm's `.idea`.

### Changed

* Documentation's front-page title is now *distribution name* (was *package name*) followed by project's one-liner.
* Documentation builds updated to the project's docs site on pushes to `master`, instead of only `tags`.

## [1.0.0] - 2019-07-16
### Changed

* Turned the project into a [cookiecutter] template.
  * Old contents moved into [`{{cookiecutter.repo_name}}/`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/{{cookiecutter.repo_name}}/)
    * Except for the CHANGELOG, which concerns the template itself.

## [0.5.0] - 2019-07-15
### Changed

* Sphinx docs improved: [automodule][sphinx.ext.autodoc] and API reference example.
* Documentation theme changed to [Read The Docs theme][sphinx-rtd-theme].
* Documentation template edited: justified text and some other minor changes.
* Line breaks in `*.md` files only after periods, in order to make it easier to read git line diffs.
* The following files are now symbolic links:
  * `docs/CHANGELOG.md` &rarr; [`CHANGELOG.md`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.5.0/CHANGELOG.md)
  * `docs/MANIFEST.md` &rarr; [`MANIFEST.md`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.5.0/MANIFEST.md)
  * `docs/README.md` &rarr; [`README.md`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.5.0/README.md)
* Moved base package to [`src/`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.5.0/src).
* Now using the `python:3.6-slim` instead of `python:3.7-slim` image for CI jobs.


## [0.4.1] - 2019-06-24
### Changed

* Distribution uploads are now managed via [Twine] in the [CI pipeline](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.4.1/.gitlab-ci.yml).


## [0.4.0] - 2019-06-24
### Added

* [`MANIFEST.md`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.4.0/MANIFEST.md) file, which is now pointed by the [README]'s introduction.
* Additional metadata in [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.4.0/pyproject.toml) (documentation, website, README, etc.).
* Package successfully built by the [CI pipeline](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.4.0/.gitlab-ci.yml) are pushed to [TestPyPI].

### Changed 

* Typo in [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.4.0/pyproject.toml).
* Package name is now `pedroasad-template-python-app`.
* [README] is now included in the documentation's landing page.


## [0.3.0] - 2019-05-17
### Changed

* Nearly fixed [License management][repository-licenses] job in CI (incompatible Python version
  prevents full fix).
* Now using the `python:3.7-slim` instead of `python:3.7-alpine` image for CI jobs.
* [Codecov] token used in pipeline now comes from protected variable.
* Documentation is only published to https://pedroasad.com.gitlab.io/templates/python/python-app tag pushes.


## [0.2.0] - 2019-05-14
### Added

* [Sphinx]-based documentation setup, which includes:
    * The [`docs/conf.py`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.2.0/docs/conf.py) configuration file,
    * The template [`docs/index.rst`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.2.0/docs/index.rst) file, and
    * A GNU [`Makefile`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.2.0/docs/Makefile).
    
### Changed

* [pre-commit][pre-commit] configuraiton now uses a hook that fixes the [README](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.2.0/README.md)'s TOC.
* [CI config](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.2.0/.gitlab-ci.yml) is now based on `python:3.6-slim`.


## [0.1.0] - 2019-05-13
### Added

* [`.bumpversion.cfg`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/.bumpversion.cfg) &mdash; Configuration file for the [bumpversion] version-tagging package.
* [`.coveragerc`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/.coveragerc) &mdash; Configuration file for the [Coverage] reporting tool.
* [`.gitignore`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/.gitignore) &mdash; List of files and directories paths/patterns [ignored by Git][gitignore].
* [`.gitlab-ci.yml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/.gitlab-ci.yml) &mdash; Continuous integration/deploy configuration ([GitLab CI]), featuring:
    * [Security verification][repository-security] of Python dependencies (via [Gitlab Dependency Scanning])
    * [License management][repository-licenses] of dependency licenses (via [Gitlab License Management])
* [`.pre-commit-config.yaml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/.pre-commit-config.yaml) &mdash; Configuration file for the [pre-commit][pre-commit] package, which aids in applying useful [Git Hooks] in team workflows. Includes:
    * Automatic code styling with [Black].
* [`CHANGELOG.md`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/CHANGELOG.md) &mdash; this very history file, which follows the [Keep a Changelog] standard.
* [`LICENSE.txt`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/LICENSE.txt) &mdash; Copy of the [MIT license] (a permissive [open source license][open-source]).
* [`README.md`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/README.md) &mdash; repository front-page.
* [`app`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/app) &mdash; Base directory of the example Python package distributed by this repository.
* [`poetry.lock`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/poetry.lock) &mdash; [Poetry]'s resolved dependency file. Whereas [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/pyproject.toml) specifies [*as abstract as possible*][post-setup-vs-requirements] dependencies, this file 
* [`pyproject.toml`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/pyproject.toml) &mdash; [PEP-517]-compliant packaging metadata, configured with the [Poetry] system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic [setup.py file][setup.py] found in *classical* Python packaging.
* [`pytest.ini`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/pytest.ini) &mdash; Configuration file for the [pytest] Python testing framework.
    * Configured for producing coverage reports (through the [pytest-cov][pip-pytest-cov] plugin).
    * Features [Codecov]-powered [coverage reports][repository-codecov]
* [`tests`](https://gitlab.com/pedroasad.com/templates/python/python-app/blob/0.1.0/tests) &mdash; [pytest]-powered test-suite.

[LICENSE]: https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/LICENSE.txt
[MANIFEST]: https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/MANIFEST.md
[README]: https://gitlab.com/pedroasad.com/templates/python/python-app/blob/master/README.md
[Unreleased]: https://gitlab.com/pedroasad.com/templates/python/python-app/compare?from=release&to=master
[1.0.0]: https://gitlab.com/pedroasad.com/templates/python/python-app/compare?from=0.5.0&to=1.0.0
[0.5.0]: https://gitlab.com/pedroasad.com/templates/python/python-app/compare?from=0.4.1&to=0.5.0
[0.4.1]: https://gitlab.com/pedroasad.com/templates/python/python-app/compare?from=0.4.0&to=0.4.1
[0.4.0]: https://gitlab.com/pedroasad.com/templates/python/python-app/compare?from=0.3.0&to=0.4.0
[0.3.0]: https://gitlab.com/pedroasad.com/templates/python/python-app/compare?from=0.2.0&to=0.3.0
[0.2.0]: https://gitlab.com/pedroasad.com/templates/python/python-app/compare?from=0.1.0&to=0.2.0
[0.1.0]: https://gitlab.com/pedroasad.com/templates/python/python-app/tags/0.1.0

[Black]: https://pypi.org/project/black/
[Codecov]: https://codecov.io
[Coverage]: https://coverage.readthedocs.io
[Git hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[Gitlab CI]: https://docs.gitlab.com/ee/ci
[Gitlab Dependency scanning]: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html
[Gitlab License Management]: https://docs.gitlab.com/ee/user/application_security/license_management/index.html
[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[MIT License]: https://opensource.org/licenses/MIT
[PEP-517]: https://www.python.org/dev/peps/pep-0517/
[Poetry]: https://github.com/sdispater/poetry
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[Sphinx]: https://www.sphinx-doc.org/en/master/index.html
[Twine]: https://twine.readthedocs.io
[Vim modelines]: https://vim.fandom.com/wiki/Modeline_magic
[bumpversion]: https://github.com/peritus/bumpversion
[cookiecutter]: https://cookiecutter.readthedocs.io
[gitignore]: https://git-scm.com/docs/gitignore 
[open-source]: https://opensource.org/
[post-setup-vs-requirements]: https://caremad.io/posts/2013/07/setup-vs-requirement/
[pre-commit]: https://pre-commit.com/
[pytest]: https://pytest.org/
[repository-codecov]: https://codecov.io/gl/pedroasad.com:templates:python/python-app
[repository-licenses]: https://gitlab.com/pedroasad.com/templates/python/python-app/licenses/dashboard
[repository-security]: https://gitlab.com/pedroasad.com/templates/python/python-app/security/dashboard
[setup.py]: https://docs.python.org/3.7/distutils/setupscript.html
[sphinx-rtd-theme]: https://sphinx-rtd-theme.readthedocs.io
[sphinx.ext.autodoc]: http://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html

