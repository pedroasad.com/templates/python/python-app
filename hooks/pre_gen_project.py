"""

Variables at user's discretion:

* cookiecutter.author_name
* cookiecutter.one_liner

Choice variables:

* cookiecutter.python_version
* cookiecutter.status
* cookiecutter.license
* cookiecutter.repo_provider
* cookiecutter.ide
"""

import re


cookiecutter_vars = {
    "cookiecutter.package_name": ("{{cookiecutter.package_name}}", r"^\w[\w\d]+$"),

    # According to https://packaging.python.org/tutorials/packaging-projects/
    "cookiecutter.distribution_name": ("{{cookiecutter.distribution_name}}", r"^[\w\d_-]+$"),

    # Obtained from https://emailregex.com/
    "cookiecutter.author_email": ("{{cookiecutter.author_email}}", r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"),

    # According to PEP-440, on
    # https://www.python.org/dev/peps/pep-0440/#appendix-b-parsing-version-strings-with-regular-expressions
    "cookiecutter.version": ("{{cookiecutter.version}}", r"^([1-9][0-9]*!)?(0|[1-9][0-9]*)(\.(0|[1-9][0-9]*))*((a|b|rc)(0|[1-9][0-9]*))?(\.post(0|[1-9][0-9]*))?(\.dev(0|[1-9][0-9]*))?$"),

    # Ad-hoc solutions that should work in most cases.
    "cookiecutter.repo_organization": ("{{cookiecutter.repo_organization}}", r"^[\w\d._-]+$"),
    "cookiecutter.repo_group": ("{{cookiecutter.repo_group}}", r"^(([\w\d._-]+)(/[\w\d._-]+)*)?$"),
    "cookiecutter.repo_name": ("{{cookiecutter.repo_name}}", r"^[\w\d._-]+$"),
}

errors = []
for key, (value, rule) in cookiecutter_vars.items():
    if not re.match(rule, value):
        errors.append(f"    * Invalid value for {key!r} variable: {value!r}. Allowed values should match regex {rule}")

if errors:
    raise ValueError("Some validation errors occurred:\n" + "\n".join(errors))
