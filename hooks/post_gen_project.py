# This script is a naughty hack! Since we need access to the vars.jinja template in the
# post-genereate script --- but cookiecutter won't give us access to the original template directory
# where this module resides (see issues 706 and 851) --- we put the actual hook script there and
# import it from here.

import sys
import os
sys.path.insert(0, os.getcwd())
import post_gen_project

