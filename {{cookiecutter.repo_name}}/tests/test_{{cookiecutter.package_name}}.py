import {{cookiecutter.package_name}}


def test_answer():
    assert {{cookiecutter.package_name}}.answer() == 42


def test_version():
    assert {{cookiecutter.package_name}}.__version__ == "{{cookiecutter.version}}"

