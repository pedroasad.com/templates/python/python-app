{% for letter in cookiecutter.distribution_name %}={% endfor %}====={% for letter in cookiecutter.one_liner %}={% endfor %}
{{cookiecutter.distribution_name}} --- {{cookiecutter.one_liner}}
{% for letter in cookiecutter.distribution_name %}={% endfor %}====={% for letter in cookiecutter.one_liner %}={% endfor %}

.. toctree::
   :maxdepth: 2

   Get started <README>
   api
   CHANGELOG
   CONTRIBUTING
   MANIFEST


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

