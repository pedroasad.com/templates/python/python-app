=============
API reference
=============

:mod:`{{cookiecutter.package_name}}`
{% for letter in cookiecutter.package_name %}={% endfor %}=======

.. automodule:: {{cookiecutter.package_name}}
    :members:

