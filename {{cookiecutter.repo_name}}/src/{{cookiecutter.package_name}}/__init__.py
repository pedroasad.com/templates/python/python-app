"""This is an example module, which contains a single function: :func:`answer`.
"""

__version__ = "{{cookiecutter.version}}"


def answer():
    """
    Returns
    -------
    a: int
        The answer.
    """
    return 42

