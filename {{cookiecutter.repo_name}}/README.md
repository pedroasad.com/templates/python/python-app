{%- import 'vars.jinja' as vars with context -%}
<!--
    vim: et: sw=4: ts=4: tw=0:
    vim: spell: spelllang=en:
-->
# {{cookiecutter.distribution_name}} &ndash; {{cookiecutter.one_liner}}

[![][badge-python]][python-docs]
[![][badge-version]][latest release]

[![][badge-mit]][MIT License]
[![][badge-black]][Black]

[![][badge-ci-status]][repository-master]
[![][badge-ci-security]][repository-security]
[![][badge-codecov]][repository-codecov]

- [Documentation]({{vars.pages_url}})
- [Issue tracker]({{vars.repo_url}}/issues)
- [Repository contents](MANIFEST.md)
- [History of changes](CHANGELOG.md)
- [Contribution/development guide](CONTRIBUTING.md)
- [Copy of MIT License](LICENSE.txt)

---

**Table of contents:**

[](TOC)

[](TOC)

---

## Installation

```bash
pip install --index-url http://test.pypi.org/simple   \
            --extra-index-url http://pypi.org/simple  \
            {{cookiecutter.distribution_name}}
```

---

- *Powered by [GitLab CI]*
- *Created by [{{cookiecutter.author_name}} &lt;{{cookiecutter.author_email}}&gt;](mailto:{{cookiecutter.author_email}}) using [cookiecutter] and [gl:pedroasad.com/templates/python/python/app@1.1.0](https://gitlab.com/pedroasad.com/templates/python/python-app/tags/1.1.0)*

[Black]: https://pypi.org/project/black/
[CHANGELOG]: ./CHANGELOG.md
[CONTRIBUTING]: ./CONTRIBUTING.md
[Gitlab CI]: https://docs.gitlab.com/ee/ci
[LICENSE]: ./LICENSE.txt
[MANIFEST]: ./MANIFEST.md
[MIT License]: https://opensource.org/licenses/MIT
[README]: {{vars.repo_url}}/blob/master/README.md
[badge-black]: https://img.shields.io/badge/code%20style-Black-black.svg
[badge-ci-coverage]: {{vars.repo_url}}/badges/master/coverage.svg
[badge-ci-security]: https://img.shields.io/badge/security-Check%20here!-yellow.svg
[badge-ci-status]: {{vars.repo_url}}/badges/master/pipeline.svg
[badge-codecov]: {{vars.codecov_url}}/branch/master/graph/badge.svg
[badge-mit]: https://img.shields.io/badge/license-MIT-blue.svg
[badge-python]: https://img.shields.io/badge/Python-%E2%89%A53.6-blue.svg
[badge-version]: https://img.shields.io/badge/version-{{cookiecutter.version}}%20{{cookiecutter.status}}-orange.svg
[cookiecutter]: http://cookiecutter.readthedocs.io/
[latest release]: https://test.pypi.org/project/{{cookiecutter.distribution_name}}/{{cookiecutter.version}}/
[python-docs]: https://docs.python.org/{{cookiecutter.python_version}}
[repository-codecov]: {{vars.codecov_url}}
[repository-master]: {{vars.repo_url}}
[repository]: {{vars.repo_url}}
[repository-security]: {{vars.repo_url}}/security

