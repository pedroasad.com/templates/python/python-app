{%- import 'vars.jinja' as vars with context -%}
<!--
    vim: et: sw=4: ts=4: tw=0:
    vim: spell: spelllang=en:
-->
# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].

## [Unreleased]

## [{{cookiecutter.version}}] - {% now 'utc', '%Y-%m-%d' %}
### Added

* [`.bumpversion.cfg`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/.bumpversion.cfg) &mdash; Configuration file for the [bumpversion] version-tagging package.
* [`.coveragerc`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/.coveragerc) &mdash; Configuration file for the [Coverage] reporting tool.
* [`.gitignore`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/.gitignore) &mdash; List of files and directories paths/patterns [ignored by Git][gitignore].
* [`.gitlab-ci.yml`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/.gitlab-ci.yml) &mdash; Continuous integration/deploy configuration ([GitLab CI]), configured for:
  * Running a [pytest]-based test suite and reporting results with [Codecov] at {{vars.codecov_url}},
  * Building the [Sphinx]-based documentation and deploying it via [Gitlab Pages] to {{vars.pages_url}}, and
  * Uploading successfully built, tagged distributions to [TestPyPI] (defaults to {{vars.testpypi_url}}).
* [`.pre-commit-config.yaml`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/.pre-commit-config.yaml) &mdash; Configuration file for the [pre-commit] package, which aids in applying useful [Git Hooks] in team workflows. Includes:
  * Automatic code styling with [Black].
* [`CHANGELOG.md`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/CHANGELOG.md) &mdash; this very history file, which follows the [Keep a Changelog] standard.
* [`LICENSE.txt`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/LICENSE.txt) &mdash; Copy of the [{{cookiecutter.license}} License].
* [`MANIFEST.md`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/MANIFEST.md) Repository's manifest.
* [`README.md`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/README.md) &mdash; repository front-page.
* [`docs/`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/docs) &mdash; [Sphinx]-based documentation setup, which includes:
  * [`conf.py`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/docs/conf.py) &mdash; [Sphinx] configuration file,
  * [`docs/index.rst`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/docs/index.rst) &mdash; documentation master file, and
  * [`Makefile`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/docs/Makefile) for building the docs easily.
* [`poetry.lock`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/poetry.lock) &mdash; [Poetry]'s resolved dependency file. Whereas [`pyproject.toml`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/pyproject.toml) specifies [*as abstract as possible*][post-setup-vs-requirements] dependencies, this file 
* [`pyproject.toml`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/pyproject.toml) &mdash; [PEP-517]-compliant packaging metadata, configured with the [Poetry] system. Includes, among other information: package qualifiers, version, author, and all of its dependencies. This file replaces the classic [setup.py] file found in *classical* Python packaging.
* [`src/{{cookiecutter.package_name}}`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/{{cookiecutter.package_name}}) &mdash; Base directory of the example Python package distributed by this repository.
* [`tests`]({{vars.repo_url}}/blob/{{cookiecutter.version}}/tests) &mdash; [pytest]-powered test-suite.

[LICENSE]: {{vars.repo_url}}/blob/master/LICENSE.txt
[MANIFEST]: {{vars.repo_url}}/blob/master/MANIFEST.md
[README]: {{vars.repo_url}}/blob/master/README.md
[Unreleased]: {{vars.repo_url}}/compare?from=release&to=master
[{{cookiecutter.version}}]: {{vars.repo_url}}/tags/{{cookiecutter.version}}
[{{cookiecutter.license}} License]: https://opensource.org/licenses/{{cookiecutter.license}}

[Black]: https://pypi.org/project/black/
[Codecov]: https://codecov.io
[Coverage]: https://coverage.readthedocs.io
[Git hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[Gitlab CI]: https://docs.gitlab.com/ee/ci
[Gitlab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[Keep a Changelog]: https://keepachangelog.com/en/1.0.0/
[PEP-517]: https://www.python.org/dev/peps/pep-0517/
[Poetry]: https://github.com/sdispater/poetry
[Poetry]: https://github.com/sdispater/poetry
[PyPI]: https://pypi.org
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
[Sphinx]: https://www.sphinx-doc.org/en/master/index.html
[TestPyPI]: https://test.pypi.org
[bumpversion]: https://github.com/peritus/bumpversion
[gitignore]: https://git-scm.com/docs/gitignore
[open-source]: https://opensource.org/
[post-setup-vs-requirements]: https://caremad.io/posts/2013/07/setup-vs-requirement/
[pre-commit]: https://pre-commit.com/
[pytest]: https://pytest.org/
[repository-codecov]: {{vars.codecov_url}}
[setup.py]: https://docs.python.org/{{cookiecutter.python_version}}/distutils/setupscript.html
