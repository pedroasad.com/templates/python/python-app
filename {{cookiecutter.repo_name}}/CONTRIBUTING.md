{%- import 'vars.jinja' as vars with context -%}
<!--
    vim: et: sw=4: ts=4: tw=0:
    vim: spell: spelllang=en:
-->
# Contribution guide

[](TOC)

[](TOC)

## Development setup

**Requirements:**

* Python &ge; {{cookiecutter.python_version}}
* [Poetry] &ge; 0.12

1. Create a [virtualenv] and install dependencies. Two of the most common options are:

    * Letting [Poetry] create the virtual environment and install dependencies for you (the default if you have just installed Poetry):
      
      ```bash
      poetry install  # This will create a virtual environment in a new subdirectory 
                      # of $(poetry config settings.virtualenvs.path) 
      ```
      
      An interesting variation is telling Poetry to create the environment inside the project's root folder by running
      
      ```bash
      poetry config settings.virtualenvs.in-project true
      ```
      
      before the `install` command.
      
    * If you have chosen to turn off automatic environment creation using
    
      ```bash
      poetry config settings.virtualenvs.create false
      ```

      then the following steps should suffice:
      
      ```bash
      virtualenv -p $(which python3.6) .venv
      source venv/bin/activate.sh
      poetry install
      ```
      
      In this case, keep in mind that you need to activate the environment using the `source` command before issuing any Poetry commands.
      You may choose a name for your virtual environment other than `.venv`, but this one is already on [`.gitignore`]({{vars.repo_url}}/blob/master/.gitignore). 
      
1. **(Optional)** Set up [pre-commit]. It is already included as *dev*-dependency in [pyproject.toml]({{vars.repo_url}}/blob/master/pyproject.toml) and a [.pre-commit-config.yaml]({{vars.repo_url}}/blob/master/.pre-commit-config.yaml) file is provided.
   Hence, you just need:
   
   ```bash
   pre-commit install
   ```


## Building the documentation

Provided that the virtual environment is active, and dependencies have been installed, it is as simple as

```bash
make html
```

and the output will be placed under `docs/_build/html`.

The [CI/CD][Gitlab CI] pipeline, through [GitLab Pages], will automatically deploy the HTML tree to {{vars.pages_url}}.
If you clone the repository and do not make further changes, documentation built after `git push` will most likely end up in https://organization-name.gitlab.io/group-name-if-any/repository-name.
   
   
## Running the tests

Provided that the virtual environment is active, and dependencies have been installed, it is as simple as

```bash
pytest
```

This already produces a code coverage report for the [`{{cookiecutter.package_name}}`]({{vars.repo_url}}/blob/master/src{{cookiecutter.package_name}}) package, stores it in a `.coverage` file and prints it.


## Publishing on PyPI or TestPyPI

You can easily publish your package to [PyPI] or [TestPyPI] using `poetry publish`, or [Twine].
For instance (assuming you already have registered for an account on [TestPyPI]), you can 

```bash
poetry config repositories.testpypi https://test.pypi.org/legacy
poetry config http-basic.testpypi TESTPYPI_USERNAME TESTPYPI_PASSWORD
```

just once; then, for every version you want to publish, run

```bash
poetry build && poetry publish -r testpypi # or
poetry publish --build -r pypi             # or
```

Alternatively, if you want to use [Twine], you can install it with `pip install twine`, then

```bash
poetry build && twine upload --repository-url https://test.pypi.org dist/*
```

which can also be made more usable by creating a `.pypirc` file.
After running `poetry build`, you can check the distribution files are ok by running `tar tvzf dist/*.tgz` and inspecting the output. If you do not configure [Poetry] for [TestPyPI] or another repository using the commands like the ones above, it will publish to default to [PyPI]. 

**Important:** You can test you project is pip-installable with

```bash
pip install --index-url http://test.pypi.org/simple   \
            --extra-index-url http://pypi.org/simple  \
            {{cookiecutter.distribution_name}}

```

If you fork this repository, set up CI variables for `TESTPYPI_USERNAME` and `TESTPYPI_PASSWORD`, or hard-code them (not recommended, for security reasons) directly in the [`.gitlab-ci.yml`]({{vars.repo_url}}/blob/master/.gitlab-ci.yml)
script.

[Gitlab CI]: https://docs.gitlab.com/ee/ci
[Gitlab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[Poetry]: https://github.com/sdispater/poetry
[PyPI]: https://pypi.org
[TestPyPI]: https://test.pypi.org
[Twine]: https://twine.readthedocs.io
[pre-commit]: https://pre-commit.com/
[pytest]: https://pytest.org/
[virtualenv]: https://virtualenv.pypa.io/en/latest/
