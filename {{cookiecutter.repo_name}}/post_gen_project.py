{%- import './vars.jinja' as vars with context -%}
import sys
import subprocess as sp

commands = [
    "rm vars.jinja",
    f"rm {__file__}",
    "git init",
    "git remote add origin git@{{cookiecutter.repo_provider}}.com:{{vars.repo_namespace}}/{{cookiecutter.repo_name}}.git",
    "git config user.name '{{cookiecutter.author_name}}'",
    "git config user.email '{{cookiecutter.author_email}}'",
    "poetry run pip install --upgrade pip",
    "poetry lock",
    "poetry install",
    "poetry run pre-commit install",
    "poetry run pre-commit run md-toc",
    "poetry run make -C docs html",
    "git add .",
]

for cmd in commands:
    sp.call(cmd.split(), stdout=sys.stdout, stderr=sys.stderr)
    
